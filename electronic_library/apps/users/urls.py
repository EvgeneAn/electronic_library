from django.urls import path

from . import views

app_name = 'accounts'

urlpatterns = [
    path('profile/', views.UserProfileView.as_view(), name='profile'),
    path('edit/profile/<int:pk>/', views.UserEditView.as_view(), name='user_edit'),
]
