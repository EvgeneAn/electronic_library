from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView
from django.views.generic.base import TemplateView

from electronic_library.mixins import *
from .models import User
from .forms import EditProfileForm


class UserProfileView(TemplateView):
    template_name = 'users/user_profile.html'


class UserEditView(OnlyUserMixin, UpdateView):
    model = User
    form_class = EditProfileForm
    template_name = 'users/user_edit.html'

    def get_success_url(self):
        return reverse_lazy('accounts:profile')
