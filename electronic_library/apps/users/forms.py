from django import forms

from .models import User


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name',
                  'patronymic', 'birthday', 'residence_place',
                  'work_study_place', 'email']
        error_messages = {
                'username': {
                    'invalid': 'Введите правильное имя пользователя. ' \
                    'Оно может содержать только буквы, цифры и знаки ' \
                    '@.+-_',
                },
                'email': {
                    'unique': 'На данную почту уже ' \
                    'зарегистрирован пользователь.'
                }
        }
