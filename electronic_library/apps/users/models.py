from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    email = models.EmailField(verbose_name='Почта',
                              blank=False, unique=True)
    patronymic = models.CharField(max_length=50,
                                  verbose_name='Отчество',
                                  blank=True, default='')
    birthday = models.DateField(verbose_name='Дата рождения',
                                blank=True, null=True, default=None)
    residence_place = models.CharField(max_length=512,
                                       verbose_name='Место проживания',
                                       blank=True, default='')
    work_study_place = models.CharField(max_length=512,
                                        verbose_name='Место учебы/работы',
                                        blank=True, default='')

    def full_name(self):
        return f'{self.first_name} {self.last_name} {self.patronymic}'

    def __str__(self):
        return str(self.id) + ': ' + self.username
