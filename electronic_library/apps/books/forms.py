from pytz import timezone
from datetime import datetime, timedelta


from django import forms
from django.core.exceptions import NON_FIELD_ERRORS

from .models import Category, Author, Book, Reservation
from electronic_library.settings import TIME_ZONE
from .validators import RangeDateTime


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name']
        error_messages = {
            'name': {
                'unique': 'Категория с таким названием уже существует.'
            }
        }


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['name', 'surname', 'patronymic', 'birthday']
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': 'Такой автор уже есть в БД.'
            }
        }

    def get_date(self):
        if self.instance:
            return self.instance.birthday.strftime('%Y-%m-%d')
        return ''


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = '__all__'


class ReaderReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        fields = ['book', 'date_start', 'date_end']


class ReservationForm(forms.Form):
    date_start = forms.DateTimeField(required=True)
    date_end = forms.DateTimeField(required=True)

    def clean_date_start(self):
        date_start = self.cleaned_data.get('date_start')
        start = datetime.now(timezone(TIME_ZONE))
        end = start + timedelta(days=1)
        RangeDateTime(start, end)(date_start)
        return date_start

    def clean_date_end(self):
        date_end = self.cleaned_data.get('date_end')
        start = self.cleaned_data.get('date_start')
        RangeDateTime(start)(date_end)
        return date_end
