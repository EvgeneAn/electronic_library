from django.urls import path

from . import views

app_name = 'books'

urlpatterns = [
    path('create/category/', views.CategoryCreateView.as_view(), name='category_create'),
    path('edit/category/<int:pk>/', views.CategoryEditView.as_view(), name='category_edit'),
    path('delete/category/<int:pk>', views.CategoryDeleteView.as_view(), name='category_delete'),
    path('show/category/<int:pk>/', views.CategoryDetailView.as_view(), name='category'),
    path('show/categories/', views.CategoryListView.as_view(), name='category_list'),

    path('create/author/', views.AuthorCreateView.as_view(), name='author_create'),
    path('edit/author/<int:pk>/', views.AuthorEditView.as_view(), name='author_edit'),
    path('delete/author/<int:pk>', views.AuthorDeleteView.as_view(), name='author_delete'),
    path('show/author/<int:pk>/', views.AuthorDetailView.as_view(), name='author'),
    path('show/authors/', views.AuthorListView.as_view(), name='author_list'),

    path('create/book/', views.BookCreateView.as_view(), name='book_create'),
    path('edit/book/<int:pk>/', views.BookEditView.as_view(), name='book_edit'),
    path('delete/book/<int:pk>', views.BookDeleteView.as_view(), name='book_delete'),
    path('show/book/<int:pk>/', views.BookDetailView.as_view(), name='book'),
    path('show/books/', views.BookListView.as_view(), name='book_list'),
    path('show/books/filter/', views.BookListFilter.as_view(), name='book_list_filter'),

    path('reservation/<int:pk>/', views.ReaderReservationView.as_view(), name='reservation'),
    path('show/reservations/', views.ReaderReservationList.as_view(), name='reservation_list'),
    path('show/reservation/<int:pk>/', views.ReaderReservationDetail.as_view(), name='reservation_detail'),
    path('show/admin/reservations/', views.AdminReservationList.as_view(), name='reservation_list_admin'),
    path('reservation/give_out/<int:pk>/', views.AdminReservationIssued.as_view(), name='reservation_give_out'),
    path('reservation/return/<int:pk>/', views.AdminReservationReturned.as_view(), name='reservation_returned'),
    path('reservation/search/', views.ReservationListFilter.as_view(), name='reservation_filter'),
]
