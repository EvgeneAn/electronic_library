from django.contrib import admin
from .models import Book, Category, Author, Reservation


class BookAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_display_links = ['name']
    search_fields = ['name']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_display_links = ['name']
    search_fields = ['name']


class AuthorAdmin(admin.ModelAdmin):
    list_display = ['name', 'surname', 'patronymic']
    list_display_links = ['name']
    search_fields = ['name', 'surname', 'patronymic']


class ReservationAdmin(admin.ModelAdmin):
    list_display = ['book', 'user', 'date_start', 'date_end']
    list_display_links = ['book', 'user']
    search_fields = ['book__name', 'user__username']


admin.site.register(Book, BookAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Reservation, ReservationAdmin)
