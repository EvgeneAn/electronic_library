from django.contrib import messages
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormView
from django.views.generic.base import View
from django.db.models import Q

from .models import Book, Category, Author, Reservation
from electronic_library.mixins import OnlyAdmin
from .forms import CategoryForm, AuthorForm, BookForm, ReservationForm


@method_decorator(login_required, 'dispatch')
class CategoryCreateView(OnlyAdmin, CreateView):
    model = Category
    form_class = CategoryForm
    template_name = 'books/category_create.html'

    def get_success_url(self):
        messages.success(self.request, 'Категория успешно добавлена!')
        return reverse_lazy('books:category', args=[self.object.id])


@method_decorator(login_required, 'dispatch')
class CategoryEditView(OnlyAdmin, UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = 'books/category_edit.html'

    def get_success_url(self):
        messages.success(self.request, 'Категория успешно отредактирована!')
        return reverse_lazy('books:category', args=[self.object.id])


@method_decorator(login_required, 'dispatch')
class CategoryDeleteView(OnlyAdmin, DeleteView):
    model = Category
    template_name = 'books/category_delete.html'

    def get_success_url(self):
        messages.success(self.request, 'Категория успешно удалена!')
        return reverse_lazy('books:categories')


@method_decorator(login_required, 'dispatch')
class CategoryDetailView(OnlyAdmin, DetailView):
    model = Category
    template_name = 'books/category_detail.html'


@method_decorator(login_required, 'dispatch')
class CategoryListView(OnlyAdmin, ListView):
    model = Category
    template_name = 'books/category_list.html'


@method_decorator(login_required, 'dispatch')
class AuthorCreateView(OnlyAdmin, CreateView):
    model = Author
    form_class = AuthorForm
    template_name = 'books/author_create.html'

    def get_success_url(self):
        messages.success(self.request, 'Автор успешно добавлен!')
        return reverse_lazy('books:author', args=[self.object.id])


@method_decorator(login_required, 'dispatch')
class AuthorEditView(OnlyAdmin, UpdateView):
    model = Author
    form_class = AuthorForm
    template_name = 'books/author_edit.html'

    def get_success_url(self):
        messages.success(self.request, 'Автор успешно отредактирован!')
        return reverse_lazy('books:author', args=[self.object.id])


@method_decorator(login_required, 'dispatch')
class AuthorDeleteView(OnlyAdmin, DeleteView):
    model = Author
    template_name = 'books/author_delete.html'

    def get_success_url(self):
        messages.success(self.request, 'Автор успешно удален!')
        return reverse_lazy('books:author_list')


@method_decorator(login_required, 'dispatch')
class AuthorDetailView(OnlyAdmin, DetailView):
    model = Author
    template_name = 'books/author_detail.html'


@method_decorator(login_required, 'dispatch')
class AuthorListView(OnlyAdmin, ListView):
    model = Author
    template_name = 'books/author_list.html'


class BookListView(ListView):
    model = Book
    template_name = 'books/book_list.html'


class BookDetailView(DetailView):
    model = Book
    template_name = 'books/book_detail.html'


class BookListFilter(ListView):
    model = Book
    template_name = 'books/book_list.html'

    def get_queryset(self):
        book_name = self.request.GET.get('book_name')
        query = Q(name__icontains=book_name)
        return Book.objects.filter(query)


@method_decorator(login_required, 'dispatch')
class BookCreateView(OnlyAdmin, CreateView):
    model = Book
    form_class = BookForm
    template_name = 'books/book_create.html'

    def get_success_url(self):
        messages.success(self.request, 'Книга успешно добавлена!')
        return reverse_lazy('books:book', args=[self.object.id])


@method_decorator(login_required, 'dispatch')
class BookEditView(OnlyAdmin, UpdateView):
    model = Book
    form_class = BookForm
    template_name = 'books/book_edit.html'

    def get_success_url(self):
        messages.success(self.request, 'Книга успешно отредактирована!')
        return reverse_lazy('books:book', args=[self.object.id])


@method_decorator(login_required, 'dispatch')
class BookDeleteView(OnlyAdmin, DeleteView):
    model = Book
    template_name = 'books/book_delete.html'

    def get_success_url(self):
        messages.success(self.request, 'Книга успешно удалена!')
        return reverse_lazy('books:book_list')


@method_decorator(login_required, 'dispatch')
class ReaderReservationView(FormView):
    form_class = ReservationForm
    template_name = 'books/reservation_reader.html'
    success_url = reverse_lazy('books:book_list')

    def get(self, request, *args, **kwargs):
        pk = request.resolver_match.kwargs.get('pk')
        book = get_object_or_404(Book, pk=pk)

        if not Reservation.unique_active_reservation(request.user, book):
            messages.warning(request,
                             f'Вы уже забронировали книгу {book.name}.')
            return HttpResponseRedirect(reverse_lazy('books:book_list'))

        if book and book.in_stock() > 0:
            return super(ReaderReservationView, self).get(request, *args,
                                                          **kwargs)

        messages.warning(request, f'Книги {book.name} нет сейчас в наличии.')
        return HttpResponseRedirect(reverse_lazy('books:book_list'))

    def post(self, request, *args, **kwargs):
        pk = request.resolver_match.kwargs.get('pk')
        book = get_object_or_404(Book, pk=pk)

        if not Reservation.unique_active_reservation(request.user, book):
            messages.warning(request,
                             f'Вы уже забронировали книгу {book.name}.')
            return HttpResponseRedirect(reverse_lazy('books:book_list'))

        if book and book.in_stock() > 0:
            form = self.get_form()
            if form.is_valid():
                Reservation.objects.create(
                    book=book,
                    user=request.user,
                    date_start=form.cleaned_data.get('date_start'),
                    date_end=form.cleaned_data.get('date_end')
                )
                return self.form_valid(form)
            else:
                return self.form_invalid(form)

        messages.warning(request, f'Книги {book.name} нет сейчас в наличии.')
        return HttpResponseRedirect(reverse_lazy('books:book_list'))


@method_decorator(login_required, 'dispatch')
class ReaderReservationList(ListView):
    model = Reservation
    template_name = 'books/reservation_list.html'

    def get_queryset(self):
        return Reservation.objects.filter(user=self.request.user)


@method_decorator(login_required, 'dispatch')
class ReaderReservationDetail(DetailView):
    model = Reservation
    template_name = 'books/reservation_detail.html'


@method_decorator(login_required, 'dispatch')
class AdminReservationList(OnlyAdmin, ListView):
    model = Reservation
    template_name = 'books/reservation_list_admin.html'

    def get_queryset(self):
        return Reservation.objects.all()


@method_decorator(login_required, 'dispatch')
class AdminReservationIssued(OnlyAdmin, View):
    """
    Выполняет смену поля received у бронирования. Используется для
    выдачи книги.
    """

    def __check_reservation(self, request, reservation, url_redirect):
        """
        Проверяет поля completed и received у бронирования, если
        книга уже выдана или возвращена, то возвращает пользователю
        соответствующие сообщение.
        """
        if reservation.completed:
            messages.warning(request,
                             f'Пользователь вернул книгу!')
            return HttpResponseRedirect(url_redirect)

        if reservation.received:
            messages.warning(request,
                             f'Книга уже выдана!')
            return HttpResponseRedirect(url_redirect)
        reservation.receive()

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        reservation = get_object_or_404(Reservation, pk=pk)
        url = reverse_lazy('books:reservation_detail', args=[int(pk)])

        self.__check_reservation(request, reservation, url)

        messages.success(request, f'Книга {reservation.book.name} выдана пользователю.')
        return HttpResponseRedirect(url)


@method_decorator(login_required, 'dispatch')
class AdminReservationReturned(OnlyAdmin, View):
    """
    Проверяет поле completed у бронирования, если книга уже
    возвращена, то возвращает сообщение пользователю.
    """

    def __check_reservation(self, request, reservation, redirect_url):
        if reservation.completed:
            messages.warning(request,
                             f'Пользователь уже вернул книгу!')
            return HttpResponseRedirect(redirect_url)
        reservation.complete()

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        reservation = get_object_or_404(Reservation, pk=pk)
        url = reverse_lazy('books:reservation_detail', args=[int(pk)])

        self.__check_reservation(request, reservation, url)
        messages.success(request, f'Книга {reservation.book.name} возвращена пользователем.')
        return HttpResponseRedirect(url)


@method_decorator(login_required, 'dispatch')
class ReservationListFilter(OnlyAdmin, ListView):
    model = Reservation
    template_name = 'books/reservation_list_admin.html'

    def get_queryset(self):
        book_name = self.request.GET.get('book_name')
        username = self.request.GET.get('username')
        query = (Q(book__name__icontains=book_name) &
                 Q(user__username__icontains=username))
        return Reservation.objects.filter(query)
