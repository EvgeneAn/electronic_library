from django.core.exceptions import ValidationError


class MinDateTime:
    """
    Проверяет, не лежит ли валидируемая дата слева от даты
    start_date.
    """
    datetime_format = '%d-%m-%Y %H:%M'

    def __init__(self, start_date=None):
        self.start_date = start_date
        self.start_date_str = ''
        if self.start_date is not None:
            self.start_date_str = self.start_date.strftime(
                self.datetime_format)

    def __call__(self, date):
        if self.start_date is not None:
            if date < self.start_date:
                raise ValidationError('Дата не может быть ранее %s' %
                                      self.start_date_str)


class MaxDateTime:
    """
    Проверяет, не лежит ли валидируемая дата справа от даты
    end_date.
    """
    datetime_format = '%d-%m-%Y %H:%M'

    def __init__(self, end_date=None):
        self.end_date = end_date
        self.end_date_str = ''
        if self.end_date is not None:
            self.end_date_str = self.end_date.strftime(
                self.datetime_format)

    def __call__(self, date):
        if self.end_date is not None:
            if date > self.end_date:
                raise ValidationError('Дата не может быть позже %s' %
                                      self.end_date_str)


class RangeDateTime:
    """
    Проверяет, лежит ли валидируемая дата в диапазоне дат
    start_date и end_date.
    """
    datetime_format = '%d-%m-%Y %H:%M'

    def __init__(self, start_date=None, end_date=None):
        """
        Инициализирует диапазон дат, в которые должна попадать
        проверяемая дата. Если end_date <= start_date, то end_date
        принимает None.
        """
        self.start_date = start_date
        self.end_date = end_date

        if self.start_date is not None and self.end_date is not None:
            if self.end_date <= start_date:
                self.end_date = None

    def __call__(self, date):
        MinDateTime(self.start_date)(date)
        MaxDateTime(self.end_date)(date)
