from django.db import models
from electronic_library.settings import AUTH_USER_MODEL


class Book(models.Model):
    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'

    name = models.CharField(verbose_name='Название',
                            max_length=256)
    authors = models.ManyToManyField('Author', verbose_name='Авторы',
                                     related_name='book_authors')
    description = models.TextField(verbose_name='Описание', blank=True,
                                   default='')
    publisher = models.CharField(verbose_name='Издательство',
                                 max_length=256)
    publishing_date = models.IntegerField(verbose_name='Дата издания')
    category = models.ForeignKey('Category', verbose_name='Категория',
                                 on_delete=models.SET_NULL,
                                 null=True, blank=True,
                                 related_name='book_category')
    count = models.IntegerField(verbose_name='Количество книг', default=0)

    def in_stock(self):
        taken = Reservation.objects.filter(book=self,
                                           completed=False).count()
        return self.count - taken

    def __str__(self):
        return self.name


class Category(models.Model):
    class Meta:
        verbose_name = 'Категория книги'
        verbose_name_plural = 'Категории книг'

    name = models.CharField(verbose_name='Название категории',
                            max_length=128, unique=True)

    def __str__(self):
        return self.name


class Author(models.Model):
    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'
        unique_together = ['name', 'surname', 'patronymic']

    name = models.CharField(verbose_name='Имя',
                            max_length=128)
    surname = models.CharField(verbose_name='Фамилия',
                               max_length=128)
    patronymic = models.CharField(verbose_name='Отчество',
                                  max_length=128)
    birthday = models.DateField(verbose_name='День рождения', default=None,
                                null=True, blank=True)

    def full_name(self):
        return self.name + ' ' + self.patronymic + ' ' + self.surname

    def __str__(self):
        return self.full_name()


class Reservation(models.Model):
    class Meta:
        verbose_name = 'Бронирование книги'
        verbose_name_plural = 'Бронирование книг'

    book = models.ForeignKey('Book', verbose_name='Книга',
                             on_delete=models.CASCADE,
                             related_name='reservation_book')
    user = models.ForeignKey(AUTH_USER_MODEL, verbose_name='Пользователь',
                             on_delete=models.CASCADE,
                             related_name='reservation_user')
    date_start = models.DateTimeField(verbose_name=
                                      'Дата начала бронирования')
    date_end = models.DateTimeField(verbose_name=
                                    'Дата окончания бронирования')
    received = models.BooleanField(verbose_name='Выдано', default=False)
    completed = models.BooleanField(verbose_name='Возвращено', default=False)
    date_created = models.DateTimeField(verbose_name='Создано',
                                        auto_now_add=True)

    @classmethod
    def unique_active_reservation(cls, user, book):
        """
        Проверяет, нет ли у пользователя активного бронирования на книгу.
        """
        reservation = Reservation.objects.filter(user=user, book=book,
                                                 completed=False)
        return False if reservation.count() > 0 else True

    def complete(self):
        if not self.completed:
            self.completed = True
            self.save()

    def receive(self):
        if not self.received:
            self.received = True
            self.save()
