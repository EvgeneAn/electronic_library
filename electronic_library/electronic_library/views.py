from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from django.contrib import messages

from .forms import RegisterForm


def home(request):
    return HttpResponseRedirect(reverse_lazy('books:book_list'))


class RegisterView(FormView):
    form_class = RegisterForm
    success_url = reverse_lazy('login')
    template_name = 'registration/register.html'

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Аккаунт успешно создан!')
        return super(RegisterView, self).form_valid(form)
