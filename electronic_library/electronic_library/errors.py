from django.shortcuts import render


def handle_page_not_found(request, exception=None):
    return render(request, 'errors/404.html', {'error': exception})


def handle_bad_request(request, exception=None):
    return render(request, 'errors/400.html', {'error': exception})


def handle_bad_permission(request, exception=None):
    return render(request, 'errors/403.html', {'error': exception})


def handle_server_error(request, exception=None):
    return render(request, 'errors/500.html', {'error': exception})
