from django.http import Http404


class OnlyUserMixin:
    """
    Допускает работу только с данными авторизованного пользователя.
    """

    def get_object(self, *args, **kwargs):
        self.object = super(OnlyUserMixin, self).get_object(*args, **kwargs)
        if self.request.user.id == self.object.id:
            return self.object
        raise Http404('Вы не можете получить данные других пользователей.')


class OnlyAdmin:
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_staff:
            return super(OnlyAdmin, self).dispatch(request, *args, **kwargs)
        raise Http404('Вы не можете получить доступ к запрашиваемому ' \
                      'ресурсу.')
