from django.contrib import admin
from django.urls import path, include

from . import views
from . import errors

urlpatterns = [
    path('', views.home, name='home'),

    path('books/', include('books.urls')),
    path('register/', views.RegisterView.as_view(), name='register'),
    path('accounts/', include('users.urls')),

    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
]

handler400 = errors.handle_bad_request
handler403 = errors.handle_bad_permission
handler404 = errors.handle_page_not_found
handler500 = errors.handle_server_error
