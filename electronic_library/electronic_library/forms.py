from django.contrib.auth.forms import UserCreationForm

from users.models import User


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'patronymic',
                  'email', 'birthday', 'residence_place', 'work_study_place',
                  'password1', 'password2']
        labels = {
            'username': 'Логин',
            'email': 'Почта'
        }
        error_messages = {
            'username': {
                'invalid': 'Введите правильное имя пользователя. ' \
                           'Оно может содержать только буквы, ' \
                           'цифры и знаки @.+-_',
                'unique': 'Пользователь с таким логином уже существует',
            },
            'email': {
                'unique': 'Пользователь с такой почтой уже существует.'
            }
        }

    error_messages = {
        'password_mismatch': 'Пароли не совпадают.',
    }
